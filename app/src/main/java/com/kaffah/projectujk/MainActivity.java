package com.kaffah.projectujk;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    private Button btnProfile;
    private Button btnMycity;
    private Button btnMyeducation;
    private Button btnMyfamily;
    private Button btnQuizz;
    private Button btnClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnProfile = (Button) findViewById(R.id.btn_profile);
        btnProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent (MainActivity.this, Profile.class);
                startActivity(i);
            }
        });
        btnMycity = (Button) findViewById(R.id.btn_mycity);
        btnMycity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent (MainActivity.this, Mycity.class);
                startActivity(i);
            }
        });
        btnMyeducation = (Button) findViewById(R.id.btn_myeducation);
        btnMyeducation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent (MainActivity.this, Myeducation.class);
                startActivity(i);
            }
        });

        btnMyfamily = (Button) findViewById(R.id.btn_myfamily);
        btnMyfamily.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent (MainActivity.this, Myfamily.class);
                startActivity(i);
            }
        });
        btnQuizz = (Button) findViewById(R.id.btn_quizz);
        btnQuizz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent (MainActivity.this, Quizz.class);
                startActivity(i);
            }


        });
        btnClose = (Button) findViewById(R.id.btn_close);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                System.exit(0);
            }});
    }
}