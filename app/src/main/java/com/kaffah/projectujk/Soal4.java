package com.kaffah.projectujk;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class Soal4 extends AppCompatActivity {
    private Button btn, btn2, btn3, btn4, btnlanjutkan, kembali;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_soal2);
        btn = (Button) findViewById(R.id.button13);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(Soal4.this, "Salah ya", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(Soal4.this, Salah.class);
                startActivity(i);
            }
        });
        btn2 = (Button) findViewById(R.id.button14);
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(Soal4.this, "Salah ya", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(Soal4.this, Salah.class);
                startActivity(i);
            }
        });
        btn3 = (Button) findViewById(R.id.button15);
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(Soal4.this, "Salah ya", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(Soal4.this, Salah.class);
                startActivity(i);
            }
        });
        btn4 = (Button) findViewById(R.id.button16);
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(Soal4.this, "Benar Sekali", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(Soal4.this, Benar.class);
                startActivity(i);
            }
        });
        
        kembali = (Button) findViewById(R.id.kembali);
        kembali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Soal4.this, MainActivity.class);
                startActivity(i);
            }
        });
    }
}