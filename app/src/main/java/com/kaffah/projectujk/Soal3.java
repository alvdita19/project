package com.kaffah.projectujk;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class Soal3 extends AppCompatActivity {
    private Button btn, btn2, btn3, btn4, btnlanjutkan, kembali;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_soal2);
        btn = (Button) findViewById(R.id.button9);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(Soal3.this, "Salah ya", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(Soal3.this, Salah.class);
                startActivity(i);
            }
        });
        btn2 = (Button) findViewById(R.id.button10);
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(Soal3.this, "Salah ya", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(Soal3.this, Salah.class);
                startActivity(i);
            }
        });
        btn3 = (Button) findViewById(R.id.button11);
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(Soal3.this, "Betul Sekali", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(Soal3.this, Benar.class);
                startActivity(i);
            }
        });
        btn4 = (Button) findViewById(R.id.button12);
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(Soal3.this, "Salah ya", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(Soal3.this, Salah.class);
                startActivity(i);
            }
        });
        btnlanjutkan = (Button) findViewById(R.id.btn_lanjutkan);
        btnlanjutkan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Soal3.this, Soal4.class);
                startActivity(i);
            }
        });
        kembali = (Button) findViewById(R.id.kembali);
        kembali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Soal3.this, MainActivity.class);
                startActivity(i);
            }
        });
    }
}